#!/bin/bash

DIVIDER="*******************************************************"

echo "$DIVIDER"
echo "Atualizando os pacotes YUM..."
echo "$DIVIDER"

sudo yum update -y

echo "$DIVIDER"
echo "Instalando Pacotes Úteis..."
echo "$DIVIDER"

sudo yum install -y nano \
                    git \
                    bind-utils

echo "Exiting..."
