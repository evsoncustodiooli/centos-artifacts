#!/bin/bash

USER_PARAM=$1

if [[ $USER_PARAM == null || $USER_PARAM == "" ]]; then
    echo -e "Usage:
        $ su
        $ Password:
        # chmod +x ./user-in-sudoers.sh
        # ./user-in-sudoers.sh <your-user>\n"
else
    permission=$(cat /etc/sudoers | grep -P "$USER_PARAM")

    if [[ $permission == "" ]]; then
        echo "Add permission..."
        echo -e "\n$USER_PARAM\tALL=(ALL)\tALL" >> /etc/sudoers
    else
        echo "You are have permission..."
        echo $permission
    fi
fi

echo "Exiting..."
