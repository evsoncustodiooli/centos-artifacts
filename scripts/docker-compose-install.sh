#!/bin/bash

DIVIDER="*******************************************************"
VERSION="1.25.4"
DOCKER_COMPOSE_HOME="/usr/local/bin/docker-compose"

if [[ $1 == "--uninstall" ]]; then
    echo "$DIVIDER"
    echo "Removendo Link Simbólico..."
    echo "$DIVIDER"

    sudo rm -f /usr/bin/docker-compose

    echo "$DIVIDER"
    echo "Removendo Docker-Compose..."
    echo "$DIVIDER"

    sudo rm $DOCKER_COMPOSE_HOME
else
    if [[ $1 != "" ]]; then
        VERSION=$1
    fi

    echo "$DIVIDER"
    echo "Baixando Docker-Compose para $DOCKER_COMPOSE_HOME"
    echo "$DIVIDER"

    sudo curl -L "https://github.com/docker/compose/releases/download/$VERSION/docker-compose-$(uname -s)-$(uname -m)" -o $DOCKER_COMPOSE_HOME

    echo "$DIVIDER"
    echo "Adicionando Permissão de Execução para $DOCKER_COMPOSE_HOME"
    echo "$DIVIDER"

    sudo chmod +x $DOCKER_COMPOSE_HOME

    echo "$DIVIDER"
    echo "Criando Link Simbólico para Todos os Usuários"
    echo "$DIVIDER"

    sudo ln -s $DOCKER_COMPOSE_HOME /usr/bin/docker-compose

    echo "$DIVIDER"
    echo "Adicionando Auto-Complete para a Linha de Comando..."
    echo "$DIVIDER"

    sudo curl -L https://raw.githubusercontent.com/docker/compose/$VERSION/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

    docker-compose --version
fi

echo "Exiting..."
