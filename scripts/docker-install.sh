#!/bin/bash

DIVIDER="*******************************************************"

echo "$DIVIDER"
echo "Removendo Pacotes Antigos do Docker..."
echo "$DIVIDER"

sudo yum remove -y docker \
                   docker-client \
                   docker-client-latest \
                   docker-common \
                   docker-latest \
                   docker-latest-logrotate \
                   docker-logrotate \
                   docker-engine

if [[ $1 == "--uninstall" ]]; then
    echo "$DIVIDER"
    echo "Desinstalando Docker..."
    echo "$DIVIDER"

    sudo yum remove -y docker-ce \
                       docker-ce-cli \
                       containerd.io

    echo "$DIVIDER"
    echo "Execute [sudo rm -rf /var/lib/docker] para excluir todas as imagens, contêiners e volumes..."
    echo "$DIVIDER"
else
    echo "$DIVIDER"
    echo "Atualizando os pacotes YUM..."
    echo "$DIVIDER"

    sudo yum update -y

    echo "$DIVIDER"
    echo "Instalando Pacotes Úteis do Docker..."
    echo "$DIVIDER"

    sudo yum install -y yum-utils \
                    device-mapper-persistent-data \
                    lvm2

    echo "$DIVIDER"
    echo "Configurando Repositório do Docker..."
    echo "$DIVIDER"

    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

    echo "$DIVIDER"
    echo "Instalando o Docker..."
    echo "$DIVIDER"

    sudo yum install -y docker-ce \
                        docker-ce-cli \
                        containerd.io

    echo "$DIVIDER"
    echo "Adicionando Permissão para [$USER]..."
    echo "$DIVIDER"

    sudo usermod -aG docker $USER

    echo "$DIVIDER"
    echo "Execute [newgrp docker] para entrar em vigor..."
    echo "$DIVIDER"

    echo "$DIVIDER"
    echo "Iniciando Serviço do Docker..."
    echo "$DIVIDER"

    sudo systemctl start docker
    sudo systemctl enable docker

    echo "$DIVIDER"
    echo "Executando Container hello-world..."
    echo "$DIVIDER"

    sudo docker run hello-world
fi

echo "Exiting..."
